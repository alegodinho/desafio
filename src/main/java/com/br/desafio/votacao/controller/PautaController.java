package com.br.desafio.votacao.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.desafio.votacao.domain.Pauta;
import com.br.desafio.votacao.dto.PautaDto;
import com.br.desafio.votacao.service.PautaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("pautas")
@RequiredArgsConstructor
@Api(value = "Criação e Votação da Pauta API")
public class PautaController {
	
	private final PautaService pautaService;
	//private final AssociadoService associadoService;
	
	@ApiOperation(value = "Criar Uma Pauta")
	@PostMapping
    public ResponseEntity<Pauta> cadastra( @RequestBody PautaDto pautaDto ){
		pautaService.save(pautaDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
	
	@GetMapping
	@ApiOperation(value = "Buscar Todas as Pautas")
    public ResponseEntity<List<Pauta>> getAll(){
        List<Pauta> list = pautaService.getAll();
        return ResponseEntity.ok(list);
    }
	

}
