package com.br.desafio.votacao.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.desafio.votacao.domain.Pauta;
import com.br.desafio.votacao.dto.PautaDto;
import com.br.desafio.votacao.exception.RegistroNaoProcessadoException;
import com.br.desafio.votacao.repository.PautaRepository;

@Service
public class PautaService {
	
	@Autowired
	private PautaRepository pautaRepository;
	
	public Pauta save(PautaDto pautaDto) {
		
		try {
			Pauta pauta = convertToModel(pautaDto);
			return pautaRepository.save(pauta);
			
		} catch (Exception ex) {
			throw new RegistroNaoProcessadoException("Pauta não registrada");
		}

	}

	private Pauta convertToModel(PautaDto pautaDto) {
		return Pauta.builder()
				.dataFim(ConverToDate(pautaDto.getDataFim()))
				.dataInicio(ConverToDate(pautaDto.getDataInicio()))
				.descricao(pautaDto.getDescricao())
				.titulo(pautaDto.getTitulo())
				.build();
	}

	private LocalDate ConverToDate(String data) {
		return LocalDate.parse(data);
	}

	public List<Pauta> getAll() {
		List<Pauta> list = pautaRepository.findAll();
		return list;
	}


}
