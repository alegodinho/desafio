package com.br.desafio.votacao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.desafio.votacao.domain.Associado;
import com.br.desafio.votacao.dto.AssociadoDto;
import com.br.desafio.votacao.repository.AssociadoRepository;

@Service
public class AssociadoService {
	
	@Autowired
	private AssociadoRepository associadoRepository;

	public Associado save(AssociadoDto associadoDto)  {

		Associado associado = convertToModel(associadoDto);
		return associadoRepository.save(associado);
	}
	

	private Associado convertToModel(AssociadoDto associadoDto) {
		
		return Associado.builder()
				.cpf(associadoDto.getCpf())
				.nome(associadoDto.getNome())
				.build();
	}

}
