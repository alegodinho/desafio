package com.br.desafio.votacao.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.br.desafio.votacao.domain.Pauta;
import com.br.desafio.votacao.dto.PautaDto;
import com.br.desafio.votacao.repository.PautaRepository;

class PautaServiceTest {
	
	@Mock
	private PautaRepository pautaRepository;
	
	@InjectMocks
	private PautaService pautaService;
	
	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	void shouldSavePauta() {
		PautaDto pautaDto = PautaDto.builder()
				.descricao("Descrição do projeto X")
				.titulo("Discussão sobre projeto X")
				.dataInicio("2022-01-01")
				.dataFim("2022-01-02")
				.build();
		
		Pauta pauta = Pauta.builder()
				.descricao("Descrição do projeto X")
				.titulo("Discussão sobre projeto X")
				.dataInicio(LocalDate.parse("2022-01-01"))
				.dataFim(LocalDate.parse("2022-01-02"))
				.build();
				
		when(pautaRepository.save(pauta)).thenReturn(pauta);
		
		Pauta savedPauta = pautaService.save(pautaDto);
		
		assertThat(savedPauta).isEqualTo(pauta);
	}
	
	@Test
	void shouldReturnListOfAllPautas() {
		Pauta pauta1 = Pauta.builder()
				.descricao("Descrição do projeto X")
				.titulo("Discussão sobre projeto X")
				.dataInicio(LocalDate.parse("2022-01-01"))
				.dataFim(LocalDate.parse("2022-01-02"))
				.build();
		
		Pauta pauta2 = Pauta.builder()
				.descricao("Descrição do projeto Y")
				.titulo("Discussão sobre projeto Y")
				.dataInicio(LocalDate.parse("2022-01-01"))
				.dataFim(LocalDate.parse("2022-01-02"))
				.build();
		
		when(pautaRepository.findAll()).thenReturn(Arrays.asList(pauta1, pauta2));
		
		List<Pauta> allPautas = pautaService.getAll();
		
		assertThat(allPautas).containsExactlyInAnyOrder(pauta1, pauta2);
	}
}