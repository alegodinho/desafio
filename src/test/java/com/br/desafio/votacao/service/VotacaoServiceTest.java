package com.br.desafio.votacao.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.br.desafio.votacao.domain.Associado;
import com.br.desafio.votacao.domain.Pauta;
import com.br.desafio.votacao.domain.Votacao;
import com.br.desafio.votacao.dto.VotosDto;
import com.br.desafio.votacao.exception.ValidacaoDeRegraDeNegocioException;
import com.br.desafio.votacao.repository.AssociadoRepository;
import com.br.desafio.votacao.repository.PautaRepository;
import com.br.desafio.votacao.repository.VotacaoRepository;

@ExtendWith(MockitoExtension.class)
public class VotacaoServiceTest {

    @Mock
    private PautaRepository pautaRepository;

    @Mock
    private VotacaoRepository votacaoRepository;

    @Mock
    private AssociadoRepository associadoRepository;

    @InjectMocks
    private VotacaoService votacaoService;

    @Test
    public void votarTest_Success() {
        //arrange
        Pauta pauta = new Pauta();
        pauta.setId(1L);
        Associado associado = new Associado();
        associado.setId(1L);
        VotosDto votoDto = new VotosDto();
        votoDto.setIdAssociado(associado.getId());
        votoDto.setIdPauta(pauta.getId());
        votoDto.setVoto("sim");
        when(pautaRepository.findById(pauta.getId()))
                .thenReturn(Optional.of(pauta));
        when(associadoRepository.findById(associado.getId()))
                .thenReturn(Optional.of(associado));
        when(votacaoRepository.findById(associado.getId()))
                .thenReturn(Optional.empty());

        //act
        votacaoService.votar(votoDto);

        //assert
        verify(votacaoRepository, times(1)).save(Votacao.builder()
        		.pauta(pauta)
        		.voto("sim")
        		.associado(associado)
        		.build());
    }

    @Test
    public void votarTest_AssociadoJaVotou() {
        //arrange
        Pauta pauta = new Pauta();
        pauta.setId(1L);
        Associado associado = new Associado();
        associado.setId(1L);
        VotosDto votoDto = new VotosDto();
        votoDto.setIdAssociado(associado.getId());
        votoDto.setIdPauta(pauta.getId());
        votoDto.setVoto("sim");
        when(pautaRepository.findById(pauta.getId()))
                .thenReturn(Optional.of(pauta));
        when(associadoRepository.findById(associado.getId()))
                .thenReturn(Optional.of(associado));
        when(votacaoRepository.findById(associado.getId()))
                .thenReturn(Optional.of(new Votacao()));

        //assert
        assertThrows(ValidacaoDeRegraDeNegocioException.class,
                () -> votacaoService.votar(votoDto));
    }

}